def qsort(list):
        if list:
            pivot = list[0]
            below = []
            above = []
            
            for i in list[1:]:
                if i < pivot:
                    below+=[i]                    
                else:
                    above+=[i]            
            return qsort(below) + [pivot] + qsort(above)
        else:
            return list

class myPowerList:
    def __init__(self):
        self.mplist = []
        
               

    def addItem(self,item):
        if isinstance(item, int):
            self.mplist.append(item)
        else:
            raise TypeError("Item should be an int")

#''''
#    helper function to validate index can be used
#''''
    def isValidIndex(self,index):
        if isinstance(index, int):
            if index >=0 and index < len(self.mplist):
                return True
        return False

    def removeItem(self,index):
        if self.isValidIndex(index):
            self.mplist.pop(index)

    


    def sortList(self):       
        self.mplist = qsort(self.mplist)
    

    def Lmerge(self,list):
        self.mplist = list + self.mplist

    def Rmerge(self,list):
        self.mplist += list

    def saveToTextFile(self,filename):
        if isinstance(filename,str):
            with open(filename,'w') as f:
                for item in self.mplist:
                    f.write("%s " % item)

    def readFromTextFile(self,filename):
        if isinstance(filename,str):
            with open(filename,'r') as f:
                for i in map(int, f.readline().split()):
                    self.addItem(i)
                                    

        



