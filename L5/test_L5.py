import os
import random
import unittest
import datasort as ds

FILE_1 = "file1.txt"
FILE_2 = "file2.csv"
FILE_3 = "file3.csv"
FILE_4 = "file4.csv"


class TestLecture5(unittest.TestCase):
    def setUp(self):
        self.test_obj = ds.Datasort()

        with open(FILE_1, "w+") as file:
            file.write("1")

        with open(FILE_2, "w+") as file:
            file.write("1, 2, 3")

        with open(FILE_3, "w+") as file:
            file.write("q, p, t")

    def test_invalid_data(self):
        self.assertRaises(ValueError, self.test_obj.set_input_data, FILE_3)

    def test_input_data_invalid_args(self):
        self.assertRaises(TypeError, self.test_obj.set_input_data, 1)
        self.assertRaises(FileNotFoundError, self.test_obj.set_input_data, "file.csv")
        self.assertRaises(ds.NotCSVFileException, self.test_obj.set_input_data, FILE_1)

    def test_input_data_valid_args(self):
        # self.test_obj = ds.Datasort()
        # with open(FILE_2, "w+") as file:
        #    file.write("1, 2, 3")
        self.test_obj.set_input_data("file2.csv")
        self.assertTrue(self.test_obj.set_input_data("file2.csv"))
        self.assertIsNotNone(self.test_obj.data)
        self.assertIsInstance(self.test_obj.data, list)
        self.assertEqual(3, len(self.test_obj.data))
        self.assertEqual(3, self.test_obj.data[2])

    def test_read_write_file(self):
        self.test_obj.set_input_data(FILE_2)
        data = self.test_obj.data
        # write data to file
        self.assertTrue(self.test_obj.set_output_data(FILE_4))
        self.assertTrue(os.path.exists(FILE_4))
        # testing reading data from file
        self.assertTrue(self.test_obj.set_input_data(FILE_4))
        self.assertEqual(data, self.test_obj.data)

    def test_merge_sort(self):
        test_cases = [
            [],
            [5],
            [-1, -3, 5, 23, 2, 34, 5, 7],
            [0, 3, 2.3, 12, 5, 2.4, 2.39],
            [9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1, 2],
            # 100 int numbers
            [random.randint(1, 100) for _ in range(100)],
            # 100 float numbers
            [random.uniform(1, 100) for _ in range(100)]
        ]

        for case in test_cases:
            self.test_obj.data = case
            self.test_obj.execute_merge_sort()
            result = self.test_obj.data

            # all elements in the original list should be in the sorted list
            self.assertTrue(len(case) == len(result))
            self.assertTrue(all([x in result for x in case]))
            # all the values should be in ASC order
            self.assertTrue(all(result[i] <= result[i + 1] for i in range(len(result) - 1)))


if __name__ == '__main__':
    unittest.main()
