import exc14
import exc15
import unittest

class TestLecture1( unittest.TestCase):
    def setUp(self):
        pass
    

    def test_emptymyPowrList(self):
        self.list1 = exc14.myPowerList()
        self.assertEqual(self.list1.mplist, [])
    
    def test_addItem(self):
        self.list1 = exc14.myPowerList()
        self.list1.addItem(1)
        self.assertEqual(self.list1.mplist, [1])
    
    def test_addIncorrectTypeItem(self):
        self.list1 = exc14.myPowerList()        
        self.assertRaises(TypeError,self.list1.addItem, "1")

    def test_removeItem(self):
        self.list1 = exc14.myPowerList()
        self.list1.addItem(1)
        self.list1.addItem(3)
        self.list1.removeItem(1)
        self.assertEqual(self.list1.mplist, [1])
        self.list1.removeItem(5)
        self.assertEqual(self.list1.mplist,[1])
    
    def test_sortList(self):
        self.list1 = exc14.myPowerList()
        self.list1.addItem(15)
        self.list1.addItem(70)
        self.list1.addItem(10)
        self.list1.addItem(3)
        self.list1.sortList()
        self.assertEqual(self.list1.mplist, [3,10,15,70])

    def test_lmerge(self):
        self.list1 = exc14.myPowerList()
        self.list1.addItem(15)
        self.list1.addItem(10)
        self.list1.addItem(3)
        self.list1.sortList()
        self.list2 = [2,78,89]
        self.list1.Lmerge(self.list2)
        self.assertEqual(self.list1.mplist, [2, 78, 89, 3, 10, 15])
    
    def test_rmerge(self):
        self.list1 = exc14.myPowerList()
        self.list1.addItem(15)
        self.list1.addItem(10)
        self.list1.addItem(3)
        self.list1.sortList()
        self.list2 = [2,78,89]
        self.list1.Rmerge(self.list2)
        self.assertEqual(self.list1.mplist, [ 3, 10, 15,2, 78, 89])
    
    def test_file(self):
        self.list1 = exc14.myPowerList()
        self.list1.addItem(15)
        self.list1.addItem(10)
        self.list1.addItem(3)
        self.list1.sortList()
        self.list2 = [2,78,89]
        self.list1.Rmerge(self.list2)
        self.list1.Lmerge(self.list2)
        self.list1.saveToTextFile('lista.txt')
        self.list3 = exc14.myPowerList()
        self.list3.readFromTextFile('lista.txt')
        self.assertEqual(self.list3.mplist,[2, 78, 89, 3, 10, 15, 2, 78, 89])
   
    def test_directory_creation(self):
        self.track = exc15.myDirectory()
        self.assertEqual(self.track.directory,[])
    
    def test_addRecord(self):
        self.track = exc15.myDirectory()
        self.node1 = exc15.DirectoryRecord('Hector','Empresarios','222','hector@oracle.com')
        self.track.addRecord(self.node1)
        self.assertEqual(self.track.directory,[self.node1])
    
    def test_searchRecord(self):
        self.track = exc15.myDirectory()
        self.node1 = exc15.DirectoryRecord('Hector','Empresarios','222','hector@oracle.com')
        self.node2 = exc15.DirectoryRecord('Christian','Patria','555','christian@oracle.com')
        self.node3 = exc15.DirectoryRecord(1,1,1,1)
        self.track.addRecord(self.node1)
        self.track.addRecord(self.node2)
        self.track.addRecord(self.node3)
        self.searchNode = self.track.search('Christian')
        self.assertEqual(self.searchNode.__str__(), "Christian Patria 555 christian@oracle.com")
    
    def test_directoryFileOps(self):
        self.track = exc15.myDirectory()
        self.node1 = exc15.DirectoryRecord('Hector','Empresarios','222','hector@oracle.com')
        self.node2 = exc15.DirectoryRecord('Christian','Patria','555','christian@oracle.com')
        self.node3 = exc15.DirectoryRecord(1,1,1,1)
        self.track.addRecord(self.node1)
        self.track.addRecord(self.node2)
        self.track.addRecord(self.node3)
        self.track.saveToFile("directorio.txt")
        self.track2 = exc15.myDirectory()
        self.track2.readFromFile("directorio.txt")
        self.searchNode = self.track.search('Christian')
        self.assertEqual(self.searchNode.__str__(), "Christian Patria 555 christian@oracle.com")
        



if __name__ == '__main__':
    unittest.main()