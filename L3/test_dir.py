#!
import Directory
import unittest


class TestDirectory(unittest.TestCase):
    def setUp(self):
        self.rec1 = Directory.Record("Alejandro", "alejandro@alejandro.com", "15", "Mexico")
        self.rec2 = Directory.Record("Alejandra", "alejandra@alejandra.com", "17", "Argentina")
        self.rec3 = Directory.Record("Hector", "hector@hotmail.com", "21", "Ecuador")
        self.dir = Directory.Directory()
        self.dir.add(self.rec1)
        self.dir.add(self.rec2)
        self.dir.add(self.rec3)

    def test_addRecord(self):
        self.assertEqual(self.dir.directory[0].name, self.rec1.name)

    def test_addBadRecord(self):
        self.assertRaises(TypeError, self.dir.add, 1)

    def test_lookUp(self):
        self.assertEqual(self.dir.lookup("Hector", "21"), self.rec3)

    def test_lookUpNoEntry(self):
        self.assertEqual(self.dir.lookup("Hector", "22"), None)

    def test_remove(self):
        self.dir.remove("Hector", "21")
        self.assertEqual(self.dir.directory, [self.rec1, self.rec2])

    def test_removeNoEntry(self):
        self.assertRaises(ValueError, self.dir.remove, "Hector", "22")

    def test_removeIncorrectValues(self):
        self.assertRaises(TypeError, self.dir.remove, 1, 1)
        self.assertRaises(TypeError, self.dir.remove, "Hector", 1)

    # def test_printDir(self):
    # self.assert

    def test_recordIncorrectAttr(self):
        self.assertRaises(TypeError, Directory.Record, 1, "alejandro@alejandro.com", "15", "Mexico")
        self.assertRaises(TypeError, Directory.Record, "alejandro@alejandro.com", 1, "15", "Mexico")
        self.assertRaises(TypeError, Directory.Record, "alejandro@alejandro.com", "15", 1, "Mexico")
        self.assertRaises(TypeError, Directory.Record, "alejandro@alejandroagriculturamurcielago.oracle.com", "15",
                          "Mexico", 1)




# Este es un comentario muy corto

if __name__ == '__main__':
    unittest.main()
