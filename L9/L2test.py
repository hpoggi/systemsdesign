
import exc15
import unittest
import logging


class TestLecture1( unittest.TestCase):
    def setUp(self):
        pass
    


   
    def test_directory_creation(self):
        self.track = exc15.myDirectory()
        self.assertEqual(self.track.directory,[])
    
    def test_addRecord(self):
        self.track = exc15.myDirectory()
        self.node1 = exc15.DirectoryRecord('Hector','Empresarios','222','hector@oracle.com')
        self.track.addRecord(self.node1)
        self.assertEqual(self.track.directory,[self.node1])
    
    def test_searchRecord(self):
        self.track = exc15.myDirectory()
        self.node1 = exc15.DirectoryRecord('Hector','Empresarios','222','hector@oracle.com')
        self.node2 = exc15.DirectoryRecord('Christian','Patria','555','christian@oracle.com')
        self.node3 = exc15.DirectoryRecord(1,1,1,1)
        self.track.addRecord(self.node1)
        self.track.addRecord(self.node2)
        self.track.addRecord(self.node3)
        self.searchNode = self.track.search('Christian')
        self.assertEqual(self.searchNode.__str__(), "Christian Patria 555 christian@oracle.com")
    
    def test_directoryFileOps(self):
        self.track = exc15.myDirectory()
        self.node1 = exc15.DirectoryRecord('Hector','Empresarios','222','hector@oracle.com')
        self.node2 = exc15.DirectoryRecord('Christian','Patria','555','christian@oracle.com')
        self.node3 = exc15.DirectoryRecord(1,1,1,1)
        self.track.addRecord(self.node1)
        self.track.addRecord(self.node2)
        self.track.addRecord(self.node3)
        self.track.saveToFile("directorio.txt")
        self.track2 = exc15.myDirectory()
        self.track2.readFromFile("directorio.txt")
        self.searchNode = self.track.search('Christian')
        self.assertEqual(self.searchNode.__str__(), "Christian Patria 555 christian@oracle.com")
        



if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,  format='%(asctime)s %(levelname)s:%(message)s')
    unittest.main()
