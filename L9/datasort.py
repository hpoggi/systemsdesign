import csv
import os
import re
import logging
log = logging.getLogger(__name__)


class Datasort:

    def set_input_data(self, file_path_name: str):
        if type(file_path_name) is not str:
            log.error('file_path_name is not a string')
            raise TypeError

        if not (os.path.exists(file_path_name) and os.path.isfile(file_path_name)):
            log.error('file_path_name does not exist')
            raise FileNotFoundError

        if not file_path_name.lower().endswith(".csv"):
            log.error('file_path_name does not have csv extension')
            raise NotCSVFileException

        self.data = []
        with open(file_path_name, newline='') as csvfile:
            try:
                dialect = csv.Sniffer().sniff(csvfile.read(1024), [',', '|'])
                log.info('file read successfully as a csv')
            except csv.Error:
                raise NotCSVFileException
            csvfile.seek(0)
            items = csv.reader(csvfile, dialect)
            log.info('file read successfully into items var')
            for row in items:
                for i in row:

                    if i.isdigit():
                        self.data.append(float(i))
                        log.info('i:%s is a digit',float(i))
                    elif re.match("^\\d+?\\.\\d+?$", i):
                        self.data.append(float(i))

                    else:
                        log.error('non numeric value found in file')
                        raise ValueError("The CSV contains non-numeric values")
        log.info('file read succesfully')
        return True

    def set_output_data(self, file_path_name: str):
        log.info('configuring output data')
        if not file_path_name.lower().endswith(".csv"):
            file_path_name = file_path_name + ".csv"
        if len(self.data) == 0:
            raise EmptyDataArrayException
        output = []
        for item in self.data:
            output.append(str(item))
        with open(file_path_name, 'w', newline='') as file:
            writer = csv.writer(file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(output)
            log.info('file written successfully')
        return True

    def execute_merge_sort(self):
        log.info('executing merge sort')
        self.data = Datasort._merge_sort(self.data)

    def _merge_sort(list):
        # This will allow us to recursively splice the input_list until there is only one element
        if len(list) > 1:

            middle = len(list) // 2
            left = list[:middle]
            right = list[middle:]

            # Divide the lists until they have one element
            left = Datasort._merge_sort(left)
            right = Datasort._merge_sort(right)

            # Merge the two lists, we'll need symmetric indexes for each list and a merged list
            merged_list = []
            i = j = 0

            while i < len(left) and j < len(right):
                if left[i] < right[j]:
                    merged_list.append(left[i])
                    i += 1
                else:
                    merged_list.append(right[j])
                    j += 1

            # If the lists were not symmetrical, we need append the remnant elements to the merged_list

            if i < len(left):
                merged_list.extend(left[i:])

            if j < len(right):
                merged_list.extend(right[j:])

            return merged_list

        else:
            # If the list contains one element, it is considered sorted
            return list


class NotCSVFileException(Exception):
    pass


class EmptyDataArrayException(object):
    pass
