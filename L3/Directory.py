class Record:

    def __init__(self, name: str, email: str, age: str, country: str):

        if not isinstance(name, str):
            raise TypeError("name must be set to a string")
        if not isinstance(email, str):
            raise TypeError("email must be set to a string")
        if not isinstance(age, str):
            raise TypeError("age must be set to a string")
        if not isinstance(country, str):
            raise TypeError("country must be set to a string")
        self.name = name
        self.email = email
        self.age = age
        self.country = country


class Directory:
    def __init__(self):
        self.directory = []

    def add(self, record):
        if not isinstance(record, Record):
            raise TypeError("object must be of type record")
        self.directory.append(record)

    def lookup(self, name, age):
        if not isinstance(name, str):
            raise TypeError("name must be set to a string")
        if not isinstance(age, str):
            raise TypeError("age must be set to a string")
        for i in self.directory:
            if i.name == name and i.age == age:
                return i

    def remove(self, name, age):
        record = self.lookup(name, age)
        self.directory.remove(record)

    def printDir(self):
        for i in self.directory:
            print(i.name, i.email, i.age, i.country)
