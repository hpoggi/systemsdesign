import unittest
import math

class Testing(unittest.TestCase):
    def test_factorial_1(self):
        self.assertEqual(math.factorial(1),1)

    def test_factorial_20(self):
        self.assertEqual(math.factorial(20),2432902008176640000)

    def test_factorial_decimal(self):
        self.assertRaises(ValueError,math.factorial,1.1)

    def test_factorial_negative(self):
        self.assertRaises(ValueError,math.factorial,-10)

    def test_factorial_letter(self):
        self.assertRaises(TypeError,math.factorial,"a")

    def test_factorial_bignum(self):
        self.assertRaises(OverflowError,math.factorial,10000000000)
       
    

if __name__ == '__main__':
    unittest.main()
