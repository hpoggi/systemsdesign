
def decToRoman(x):
    try:
        
        if type(x) != int:
            raise
    except:
        return 'Value is not an integer'
    
    if x >= 4000 :
        return 'Value higher than 3999'
    if x <= 0 :
        return 'Value equal or lower than 0'
    res=''
    while x != 0 :
        if x >= 1000:
            res+='M'
            x-=1000
        elif x>=900:
            res+='CM'
            x-=900
        elif x>=500:
            res+='D'
            x-=500
        elif x>=400:
            res+='CD'
            x-=400
        elif x>=100:
            res+='C'
            x-=100
        elif x>=90:
            res+='XC'
            x-=90
        elif x>=50:
            res+='L'
            x-=50
        elif x>=40:
            res+='XL'
            x-=40
        elif x>=10:
            res+='X'
            x-=10
        elif x>=9:
            res+='IX'
            x-=9
        elif x>=5:
            res+='V'
            x-=5
        elif x>=4:
            res+='IV'
            x-=4
        elif x>=1:
            res+='I'
            x-=1
    return res


