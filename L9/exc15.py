import logging
log = logging.getLogger(__name__)
class DirectoryRecord:
    def __init__(self,name,address,phone,email):
        "constructor to initiate this object"
        log.info('Record created')
        self.name = str(name)        
        self.address = str(address)
        self.phone = str(phone)
        self.email = str(email)
    
    def __str__(self):
        log.info('record to string')
        return (self.name+' '+self.address+' '+self.phone+' '+self.email)
    def toFile(self):
        log.info('record to file format')
        return ('\"'+self.name+'\",'+'\"'+self.address+'\",'+'\"'+self.phone+'\",'+'\"'+self.email+'\"\n')

        
 
    
    
    
class myDirectory:
    def __init__(self):     
        self.directory=[]
        log.info('Directory created')
		
    def addRecord(self,item):
        log.info('Record created: %s',item)
        if not isinstance(item, DirectoryRecord):
            log.error('item: %s is not instance of DirectoryRecord', item)
            raise Exception('item is not of type DirectoryRecord')
        self.directory.append(item)
        return(self.directory)
        
    def search(self,name):
        log.info('Searching for record: %s',name)
        for record in range (0, len(self.directory),1):
            if(name in self.directory[record].name):
                log.info('Record %s found',name)
                return(self.directory[record])
        log.info('Record %s not found',name)
        return("This name is not in the directory")

    def saveToFile(self,filename):
        log.info('Saving into file: %s',filename)
        if isinstance(filename,str):
            with open(filename,'w') as f:
                for item in self.directory:
                    f.write("%s"%item.toFile())

    def readFromFile(self,filename):
        log.info('Reading from file: %s',filename)
        if isinstance(filename,str):
            with open(filename,'r') as f:
                for i in map(str, f.read().splitlines()):
                    for j in map(str, i.split()):
                        print(j.replace('\"',''))

        
    

       
        

