'''
    Hector Poggi
    mean receives a data set (data) and returns the mean
'''  
def mean(data):
    res = 0
    for i in range(0,len(data)):
        res += data[i]
    res /= len(data)
    return float(round(res,4))
'''
    stdDev receives a data set (data) and returns the standard deviation
'''   
def stdDev(data):
    meanRes=mean(data)
    res = 0
    for i in range(0,len(data)):
        res += (data[i]-meanRes)**2
    res /= (len(data)-1)
    res = res**.5
    return float(round(res,4))
'''
    median receives a data set (data) and returns the median
'''   
def median(data):
    data.sort()
    middle=len(data)/2
    if(middle%2 != 0):        
        mid=int(middle)
        return data[mid-1]
    else:
        mid=int(middle)
        return (data[mid]+data[mid-1])/2
'''
    percentile receives a data set(data) and returns a set of values(first quartile, second quartile , third quartile)
'''   
def quartile(data):
    data.sort()
    q1 = 0
    q2 = 0
    q3 = 0
    pos=int(len(data)/4)
    
    pos2 = int(len(data)/4*3)  
    
    q1 = data[pos] - (0.25 * (data[pos] - data[pos-1]))
    q2 = median(data)
    q3 = data[pos2] - (0.75 * (data[pos2] - data[pos2-1]))
    
    '''
    if(pos%2 != 0):        
        pos = int(pos)
        q1 = data[pos]
        
    else:
        pos = int(pos)
        q1 = (data[pos]+data[pos+1])/2
    '''
    
    return q1,q2,q3


